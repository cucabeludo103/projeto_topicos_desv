<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrinho de Compras</title>
    </head>
    <body>
        <form action="/trabalho1_topicos/Adicionar">
            <label for="quantidadeid">Quantidade</label>
            <input type="text" id="quantidadeid" name="quantidade">
            <select name="produto" id="produto">
                <c:forEach var="produto" items="${produtos}">
                    <option value="${produto.codigo}">${produto.nome}</option>
                </c:forEach>
            </select>
            <input type="submit" value="Adicionar no Carrinho" id="adicionar">
            <br><br>
        </form>
        <c:forEach var="item" items="${carrinho.itens}" varStatus="status">
            ${item.quantidade} ${item.produto.nome}
            <br>
            <a href="/trabalho1_topicos/Aumentar?posicao=${status.count}">Aumentar</a>  
            <a href="/trabalho1_topicos/Diminuir?posicao=${status.count}">Diminuir</a> 
            <a href="/trabalho1_topicos/Remover?posicao=${status.count}">Remover</a> 
            <br><br>    
        </c:forEach>
        <form action="/trabalho1_topicos/FinalizarCompra">
            <input type="submit" value="Finalizar Pedido" id="finalizar"/>
        </form>
    </body>
</html>
