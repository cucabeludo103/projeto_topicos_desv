<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Todos Pedidos</title>
    </head>
    <body>
        <h1>Pedidos Efetuados</h1>
        <c:forEach var="pedido" items="${pedidos}">
            <br>
            <p>Pedido: ${pedido.codigo}</p>
            <c:forEach var="item" items="${pedido.itens}">
                <p>Código Produto: ${item.produto.codigo}</p>
                <p>Quantidade ${item.quantidade}</p>
                <p>Nome Produto: ${item.produto.nome}</p>
                <p>Descrição Produto: ${item.produto.descricao}</p>
                <p>Preço Produto: ${item.produto.preco}</p>
            </c:forEach>
            <br><br>
        </c:forEach>
        <a href="/trabalho1_topicos/index.html">Voltar</a>
    </body>
</html>
