package controle;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CarrinhoCompras;
import modelo.Pedido;
import modelo.dao.DAOFactory;
import modelo.dao.PedidoDAO;
import modelo.dao.ProdutoDAO;


public class ServletControleVendas extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String caminho = request.getServletPath();
        if (caminho.equals("/MostrarPedidos")) {
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                PedidoDAO dao = factory.criarPedidoDAO();
                request.setAttribute("pedidos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/MostrarPedidos.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/FazerCompra")) {
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                ProdutoDAO dao = factory.criarProdutoDAO();
                request.setAttribute("produtos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/EfetuarCompra.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/Adicionar")) {
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                int quantidade = Integer.parseInt(request.getParameter("quantidade"));
                long codigoProduto = Long.parseLong(request.getParameter("produto"));
                ProdutoDAO dao = factory.criarProdutoDAO();
                
                CarrinhoCompras carrinho = (CarrinhoCompras) request.getSession().getAttribute("carrinho");
                if (carrinho == null) {
                    carrinho = new CarrinhoCompras();
                }
                carrinho.adicionarItem(quantidade, dao.buscar(codigoProduto));
                request.getSession().setAttribute("carrinho", carrinho);
                request.setAttribute("produtos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/EfetuarCompra.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/Aumentar")) {
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                int posicao = Integer.parseInt(request.getParameter("posicao"));
                CarrinhoCompras carrinho = (CarrinhoCompras) request.getSession().getAttribute("carrinho");
                carrinho.aumentarQuantidade(posicao - 1);
                request.getSession().setAttribute("carrinho", carrinho);
                ProdutoDAO dao = factory.criarProdutoDAO();
                request.setAttribute("produtos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/EfetuarCompra.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/Diminuir")) {
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                int posicao = Integer.parseInt(request.getParameter("posicao"));
                CarrinhoCompras carrinho = (CarrinhoCompras) request.getSession().getAttribute("carrinho");
                carrinho.diminuirQuantidade(posicao - 1);
                request.getSession().setAttribute("carrinho", carrinho);
                ProdutoDAO dao = factory.criarProdutoDAO();
                request.setAttribute("produtos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/EfetuarCompra.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/Remover")) {   
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                int posicao = Integer.parseInt(request.getParameter("posicao"));
                CarrinhoCompras carrinho = (CarrinhoCompras) request.getSession().getAttribute("carrinho");
                carrinho.removerItem(posicao - 1);
                request.getSession().setAttribute("carrinho", carrinho);
                ProdutoDAO dao = factory.criarProdutoDAO();
                request.setAttribute("produtos", dao.buscarTodos());
                RequestDispatcher rd = request.getRequestDispatcher("/EfetuarCompra.jsp");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/FinalizarCompra")) {   
            DAOFactory factory = new DAOFactory();
            try {
                factory.abrirConexao();
                CarrinhoCompras carrinho = (CarrinhoCompras) request.getSession().getAttribute("carrinho");
                Pedido pedido = new Pedido();
                pedido.gerarAPartirDoCarrinhoCompras(carrinho);
                pedido.setData("09/11/2016");
                PedidoDAO dao = factory.criarPedidoDAO();
                dao.gravar(pedido);
                carrinho.esvaziar();
                RequestDispatcher rd = request.getRequestDispatcher("/index.html");
                rd.forward(request, response);
                factory.fecharConexao();
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
