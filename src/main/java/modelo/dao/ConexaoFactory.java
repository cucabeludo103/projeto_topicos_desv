package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexaoFactory {

    public static Connection getConexao() throws SQLException {
        String caminho = "jdbc:mysql";
//        String caminho = "jdbc:postgresql";
        String host = "localhost";
        String porta = "3306";
//        String porta = "5432";
        String bd = "controlevendas";
        String login = "root";
//        String login = "postgres";
        String senha = "12345";  // admwindows, admlinux ou vazia “”
        String url = caminho + "://" + host + ":" + porta + "/" + bd;

        Connection conexao = null;

        try {
            System.out.println("Conectando com o banco de dados.");
            Class.forName("com.mysql.jdbc.Driver");
//            Class.forName("org.postgresql.Driver");
            conexao = DriverManager.getConnection(url, login, senha);
            System.out.println("Conexão com o banco de dados estabelecida.");
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao carregar o driver JDBC.");
        }
        return conexao;
    }

    public static Connection getConexaoH2Memoria() throws SQLException {
        String bd = "controlevendas";
        String url = "jdbc:h2:mem:" + bd + ";DATABASE_TO_UPPER=false;DB_CLOSE_DELAY=-1;"
                + "INIT=CREATE TABLE IF NOT EXISTS produto (codigo bigint(20) "
                + "PRIMARY KEY AUTO_INCREMENT, nome varchar(255), "
                + "descricao varchar(255), preco double)\\;";
        String login = "root";
        String senha = "12345";
        boolean criarBD = false;
        Connection conexao = null;
        try {
            Class.forName("org.h2.Driver");
            conexao = DriverManager.getConnection(url, login, senha);
        } catch (SQLException e) {
            System.out.println("Não foi possível estabelecer a conexão com o banco selecionado.");
            DAOFactory.mostrarSQLException(e);
        } catch (ClassNotFoundException ex) {
            System.out.println("Não foi possível carregar o driver JDBC do BD.");
        }
        return conexao;
    }

    private static void criarTabelas(Connection conexao) throws SQLException {
        String create = "CREATE TABLE produto (codigo bigint(20) "
                + "PRIMARY KEY AUTO_INCREMENT, nome varchar(255), "
                + "descricao varchar(255), preco double);";
        try (Statement stmt = conexao.createStatement()) {
            int resultado = stmt.executeUpdate(create);
        }
    }

    private static void inserirDadosIniciais(Connection conexao) throws SQLException {
        String insert = "INSERT INTO produto (nome, descricao, preco) "
                + "VALUES('teste', 'teste', '30');";
        try (Statement stmt = conexao.createStatement()) {
            int resultado = stmt.executeUpdate(insert);
        }
    }

    private ConexaoFactory() {
    }
}
