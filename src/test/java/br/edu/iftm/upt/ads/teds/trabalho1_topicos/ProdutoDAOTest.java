/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.teds.trabalho1_topicos;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import java.sql.SQLException;
import modelo.dao.DAOFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import modelo.Produto;
import modelo.dao.ProdutoDAO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 *
 * @author ads2016026p
 */
public class ProdutoDAOTest {
    
    /*public ProdutoDAOTest() {
    }
       private static final DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private DAOFactory factory;
    private ProdutoDAO dao;

    @Before
    public void setUp() throws SQLException {
        Operation operation
                = sequenceOf(OperacoesComunsBD.LIMPA_TUDO,
                        //OperacoesComunsBD.RESET_AUTOINCREMENT_MYSQL,
                        OperacoesComunsBD.RESET_AUTOINCREMENT_H2,
                        OperacoesComunsBD.INSERE_DADOS_BASICOS);
        
        
        
        

        DbSetup dbSetup = new DbSetup(new DriverManagerDestination(
                "jdbc:h2:mem:controlevendas;DATABASE_TO_UPPER=false;DB_CLOSE_DELAY=-1;"
                + "INIT=CREATE TABLE IF NOT EXISTS produto (codigo bigint(20) "
                + "PRIMARY KEY AUTO_INCREMENT, nome varchar(255), "
                + "descricao varchar(255), preco double)\\;",
                "root", "12345"), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        factory = new DAOFactory();
        factory.abrirConexao();
        dao = factory.criarProdutoDAO();
    }

    @After
    public void tearDown() throws SQLException {
        factory.fecharConexao();
    }

    @Test
    public void testGravar() throws Exception {
        Produto produto = new Produto();
        Produto produto2 = new Produto();
        produto.setDescricao("1");
        produto.setNome("1");
        produto.setPreco(1);
        dao.gravar(produto);
        produto2 = dao.buscar(produto.getCodigo());
        assertEquals(produto.getNome(), produto2.getNome());
    }

    @Test
    public void testBuscar() throws SQLException {
        String nome = "PS4";
        Produto produto = new Produto();
        produto = dao.buscar(2);
        assertEquals(nome, produto.getNome());
    }

    @Test
    public void testBuscarIdInexistente() throws SQLException {
        assertNull(dao.buscar(5));
    }

    @Test
    public void testBuscarTodos() throws SQLException {
        assertNotNull(dao.buscarTodos());
    }*/
}
