/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.teds.trabalho1_topicos;

import java.util.List;
import modelo.CarrinhoCompras;
import modelo.Item;
import modelo.Pedido;
import modelo.Produto;
import static org.junit.Assert.assertNotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 *
 * @author ads2016026p
 */
public class testeUnitario {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private List<Produto> produtos;

    @Mock
    private List<Item> itens;

    @Mock
    private CarrinhoCompras carrinho;


    @Test(expected = NullPointerException.class)
    public void deveAdicionarItem() {
        Pedido pedido = new Pedido();
        produtos.add(new Produto(1, "qualque", "coisa 1", 1));
        pedido.adicionarItem(3, produtos.get(1));

        assertNotNull(pedido.getItens());
    }
    
    public void deveSetarArrayDeItens() {
        Pedido pedido = new Pedido();
        
        produtos.add(new Produto(1, "qualque", "coisa 1", 1));
        produtos.add(new Produto(2, "qualque", "coisa 2", 2));
        produtos.add(new Produto(3, "qualque", "coisa 3", 3));
        
        for (Produto p : produtos){
            itens.add(new Item(3, p));
        }
        
        pedido.setItens(itens);
        
        assertNotNull(pedido.getItens());
    }

    public void deveGerarAPartirDoCarrinhoCompras() {
        Pedido pedido = new Pedido();
        
        produtos.add(new Produto(1, "qualque", "coisa 1", 1));
        produtos.add(new Produto(2, "qualque", "coisa 2", 2));
        produtos.add(new Produto(3, "qualque", "coisa 3", 3));
        
        for (Produto p : produtos){
             carrinho.adicionarItem(1, p);
        }
        
        pedido.gerarAPartirDoCarrinhoCompras(carrinho);
        
        assertNotNull(pedido.getItens());
    }
 
    public testeUnitario() {
    }

}
