/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.teds.trabalho1_topicos;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import static java.lang.Thread.sleep;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TesteSelenium {

    private static WebDriver driver;
  

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        driver.get("localhost:8084/trabalho1_topicos");
    }

    @AfterClass
    public static void tearDownClass() {
        if (driver != null) {
            driver.quit();
        }
    }



    @Test
    public void deveConseguirRealizarCompra() throws Exception{ 
    new WebDriverWait(driver, 3).until(ExpectedConditions.visibilityOfElementLocated(By.id("comprar")));        
        WebElement botao = driver.findElement(By.id("comprar"));
        botao.submit();
        
        WebElement quant = driver.findElement(By.id("quantidadeid"));
        quant.sendKeys("3");
        
        Select dropdown = new Select(driver.findElement(By.id("produto")));
        dropdown.selectByIndex(0);
        
        WebElement add  = driver.findElement(By.id("adicionar"));
        add.click();
        
        WebElement finalizar = driver.findElement(By.id("finalizar"));
        finalizar.submit();   

        Thread.sleep(3000);
    }
    
    @Test
    public void deveConseguirExibirVendas() throws InterruptedException {  
        
        WebElement botao = driver.findElement(By.id("ver"));
        botao.submit();

        Thread.sleep(3000);
    }
}
