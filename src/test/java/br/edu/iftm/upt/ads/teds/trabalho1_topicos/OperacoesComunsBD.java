/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.teds.trabalho1_topicos;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static com.ninja_squad.dbsetup.Operations.sql;
import com.ninja_squad.dbsetup.generator.ValueGenerators;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ads2016026p
 */
public class OperacoesComunsBD {
   /*  
        public static final Operation LIMPA_TUDO
            = deleteAllFrom("produto");
    public static final Operation RESET_AUTOINCREMENT_MYSQL
            = sql("ALTER TABLE produto AUTO_INCREMENT = 1");

    public static final Operation RESET_AUTOINCREMENT_H2
            = sql("ALTER TABLE produto ALTER COLUMN codigo RESTART WITH 1");

    public static final Operation INSERE_DADOS_BASICOS
            = sequenceOf(
                    insertInto("produto")
                    .columns("nome", "descricao", "preco")
                    .values("Pen-Drive", "USB", "30")
                    .values("PS4", "Video Game", "2000")
                    .values("PC Positivo", "Computador potente", "1000")
                    .build());

    public static final Operation INSERE_DADOS_BASICOS_IDS_ESPECIFICOS
            = sequenceOf(
                    insertInto("produto")
                    .withGeneratedValue("idContato",
                            ValueGenerators.sequence().startingAt(1).incrementingBy(1))
                    .columns("nome", "descricao", "preco")
                    .values("Pen-Drive", "USB", "30")
                    .values("PS4", "Video Game", "2000")
                    .values("PC Positivo", "Computador potente", "1000")
                    .build());
    public static final Operation INSERE_DADOS_REPETIDOS
            = sequenceOf(
                    insertInto("produto")
                    .withGeneratedValue("condigo",
                            ValueGenerators.sequence().startingAt(1).incrementingBy(1))
                    .withGeneratedValue("nome",
                            ValueGenerators.stringSequence("nome").startingAt(1))
                    .withGeneratedValue("descricao",
                            ValueGenerators.stringSequence("descricao").startingAt(1))
                    .withGeneratedValue("preco",
                            ValueGenerators.stringSequence("preco").startingAt(1))
                    .build());

    public OperacoesComunsBD() {
    }
    */
}
